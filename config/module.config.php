<?php
namespace paml\Auth\Facebook;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;

return [
    'controllers' => [
        'factories' => [
            Controller\AuthFacebookController::class => Factory\AuthFacebookControllerFactory::class,
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => AnnotationDriver::class,
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Entity'],
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                ],
            ],
        ],
    ],
    'router' => [
        'routes' => [
            'auth' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/auth',
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'facebook' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/facebook[/:action]',
                            'defaults' => [
                                'controller' => Controller\AuthFacebookController::class,
                                'action' => 'index',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            Repository\ExtendedUserRepository::class => Factory\ExtendedUserFacebookRepositoryFactory::class,
            Service\AuthFacebookService::class => Factory\AuthFacebookServiceFactory::class,
            Service\AuthFacebookAdapter::class => \paml\Auth\Factory\Service\AuthAdapterFactory::class,
        ],
    ],
    'session_containers' => [
        'Route\Session'
    ],
    'view_helpers' => [
        'factories' => [
            Helper\AuthFacebookUrlHelper::class => Factory\AuthFacebookUrlHelperFactory::class,
        ],
        'aliases' => [
            'facebookUrl' => Helper\AuthFacebookUrlHelper::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'paml\Auth\Facebook' => __DIR__ . '/../view',
        ],
    ],
];
