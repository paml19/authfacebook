<?php

namespace paml\Auth\Facebook\Controller;

use paml\Auth\Exception\AuthorizationException;
use paml\Auth\Facebook\Service\AuthFacebookAdapter;
use paml\Auth\Facebook\Service\AuthFacebookService;
use paml\Auth\Facebook\Service\UserAndHistory;
use paml\Auth\Listener\HistoryListener;
use paml\Auth\Module;
use paml\Auth\Service\AuthManager;
use Zend\Authentication\Result;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;

class AuthFacebookController extends AbstractActionController
{
    private $authFacebookService;

    private $authManager;

    private $authFacebookAdapter;

    private $container;

    public function __construct(
        AuthFacebookService $authFacebookService,
        AuthManager $authManager,
        AuthFacebookAdapter $authFacebookAdapter,
        Container $container
    ) {
        $this->authFacebookService = $authFacebookService;
        $this->authManager = $authManager;
        $this->authFacebookAdapter = $authFacebookAdapter;
        $this->container = $container;
    }

    public function callbackAction()
    {
        $code = $this->authFacebookService->handleRequest($this->params());
        $oauthAccessToken = $this->authFacebookService->getAccessToken($code);
        $userAndHistory = $this->authFacebookService->createUser($oauthAccessToken);
        return $this->login($userAndHistory);
    }

    private function login(UserAndHistory $userAndHistory)
    {
        $user = $userAndHistory->getUser();
        $historyType = $userAndHistory->getHistoryType();

        if ($historyType) {
            $this->getEventManager()->trigger(
                Module::AUTH_USER_HISTORY,
                $this,
                [
                    'userId' => $user->getUser()->getId(),
                    'type' => $historyType,
                    'success' => true,
                ]
            );

            $this->getEventManager()->trigger(
                Module::AUTH_USER_REGISTERED_IN,
                $this,
                ['user' => $user->getUser()]
            );
        }

        $result = $this->authManager->login(
            $user->getUser()->getEmail(),
            '',
            true,
            $this->authFacebookAdapter
        );

        if ($result->getCode() == Result::SUCCESS) {
            $this->getEventManager()->trigger(
                Module::AUTH_USER_HISTORY,
                $this,
                [
                    'userId' => $this->authManager->getIdentity()['id'],
                    'type' => HistoryListener::HISTORY_TYPE_LOGIN,
                    'success' => true,
                ]
            );

            if (isset($this->sessionContainer->route)) {
                return $this->redirect()->toUrl($this->sessionContainer->route);
            }

            return $this->redirect()->toRoute('home');
        } else {
            $this->getEventManager()->trigger(
                Module::AUTH_USER_HISTORY,
                $this,
                [
                    'userLogin' => $user->getUser()->getEmail(),
                    'type' => HistoryListener::HISTORY_TYPE_LOGIN
                ]
            );

            throw new AuthorizationException('Not logged, result: ' . $result->getMessages()[$result->getCode()], 401);
        }
    }
}
