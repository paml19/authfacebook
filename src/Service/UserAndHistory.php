<?php

namespace paml\Auth\Facebook\Service;

use paml\Auth\Facebook\Entity\User;

class UserAndHistory
{
    private $user;

    private $historyType;

    public function __construct(User $user, ?string $historyType)
    {
        $this->user = $user;
        $this->historyType = $historyType;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getHistoryType(): ?string
    {
        return $this->historyType;
    }
}
