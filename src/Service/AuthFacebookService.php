<?php

namespace paml\Auth\Facebook\Service;

use League\OAuth2\Client\Provider\Facebook;
use paml\Auth\Facebook\Entity\AccessToken;
use paml\Auth\Facebook\Entity\User;
use paml\Auth\Facebook\Repository\ExtendedUserRepository;
use paml\Auth\Facebook\Repository\UserRepository;
use paml\Auth\Form\Constants\Register;
use paml\Auth\Listener\HistoryListener;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Session\Container;
use paml\Auth\Repository\UserRepository as BaseUserRepository;
use League\OAuth2\Client\Token\AccessToken as OAuthAccessToken;
use paml\Auth\Entity\User as BaseUser;

class AuthFacebookService
{
    private $facebook;

    private $container;

    private $baseUserRepository;

    private $userRepository;

    private $extendedUserRepository;

    public function __construct(
        Facebook $facebook,
        Container $container,
        BaseUserRepository $baseUserRepository,
        UserRepository $userRepository,
        ExtendedUserRepository $extendedUserRepository
    )
    {
        $this->facebook = $facebook;
        $this->container = $container;
        $this->baseUserRepository = $baseUserRepository;
        $this->userRepository = $userRepository;
        $this->extendedUserRepository = $extendedUserRepository;
    }

    public function createAuth(): string
    {
        $authUrl = $this->facebook->getAuthorizationUrl([
            'scope' => [
                'email',
            ]
        ]);

        return $authUrl;
    }

    public function handleRequest(Params $requestParams): string
    {
        $error = $requestParams->fromQuery('error', null);
        $code = $requestParams->fromQuery('code', null);

        if ($error) {
            throw new \Exception('Error: ' . htmlspecialchars($error, ENT_QUOTES, 'UTF-8'));
        }

        return $code;
    }

    public function getAccessToken(string $code): OAuthAccessToken
    {
        $token = $this->facebook->getAccessToken('authorization_code', [
            'code' => $code,
        ]);

        return $token;
    }

    public function createUser(OAuthAccessToken $token): UserAndHistory
    {
        try {
            $ownerDetails = $this->facebook->getResourceOwner($token);
            $formData = [
                Register::NAME => $ownerDetails->getFirstName(),
                Register::SURNAME => $ownerDetails->getLastName(),
                Register::EMAIL => $ownerDetails->getEmail()
            ];

            $historyType = null;

            $baseUser = $this->baseUserRepository->findOneBy(['email' => $formData[Register::EMAIL]]);

            if (! $baseUser) {
                $baseUser = (new BaseUser())
                    ->setName($formData[Register::NAME])
                    ->setSurname($formData[Register::SURNAME])
                    ->setEmail($formData[Register::EMAIL]);

                $historyType = HistoryListener::HISTORY_TYPE_REGISTER;
            }

            $baseUser->setActive(true);

            $user = current($this->userRepository->findByUserEmail($formData[Register::EMAIL]));

            if (! $user) {
                $user = (new User())
                    ->setUser($baseUser);
            }

            $accessToken = (new AccessToken())
                ->setAccessToken($token->getToken())
                ->setDateExpire($token->getExpires());

            $user->addAccessToken($accessToken);

            $this->extendedUserRepository->saveFacebookWithDefaultRole($user);

            return (new UserAndHistory($user, $historyType));
        } catch (\Exception $e) {
            throw new $e;
        }
    }
}
