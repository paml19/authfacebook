<?php

namespace paml\Auth\Facebook\Helper;

use paml\Auth\Facebook\Service\AuthFacebookService;
use Zend\View\Helper\AbstractHelper;

class AuthFacebookUrlHelper extends AbstractHelper
{
    private $authFacebookService;

    public function __construct(AuthFacebookService $authFacebookService)
    {
        $this->authFacebookService = $authFacebookService;
    }

    public function __invoke()
    {
        return $this->authFacebookService->createAuth();
    }
}
