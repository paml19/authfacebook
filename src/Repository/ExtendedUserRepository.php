<?php

namespace paml\Auth\Facebook\Repository;

use paml\Auth\Facebook\Entity\User;
use paml\Auth\Repository\ExtendedUserRepository as BaseExtendedUserRepository;

class ExtendedUserRepository extends BaseExtendedUserRepository
{
    public function saveFacebookWithDefaultRole(User $userFacebook): void
    {
        if (! $userFacebook->getId()) {
            $this->saveWithDefaultRole($userFacebook->getUser());
        }

        $this->getEntityManager()->persist($userFacebook);
        $this->getEntityManager()->flush($userFacebook);
    }
}
