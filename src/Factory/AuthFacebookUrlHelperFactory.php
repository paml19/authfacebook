<?php

namespace paml\Auth\Facebook\Factory;

use Interop\Container\ContainerInterface;
use paml\Auth\Facebook\Helper\AuthFacebookUrlHelper;
use paml\Auth\Facebook\Service\AuthFacebookService;
use Zend\ServiceManager\Factory\FactoryInterface;

class AuthFacebookUrlHelperFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new AuthFacebookUrlHelper(
            $container->get(AuthFacebookService::class)
        );
    }
}
