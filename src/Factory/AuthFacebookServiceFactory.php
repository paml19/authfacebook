<?php

namespace paml\Auth\Facebook\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use League\OAuth2\Client\Provider\Facebook;
use paml\Auth\Facebook\Entity\User;
use paml\Auth\Entity\User as BaseUser;
use paml\Auth\Facebook\Repository\ExtendedUserRepository;
use paml\Auth\Facebook\Service\AuthFacebookService;
use Zend\ServiceManager\Factory\FactoryInterface;

class AuthFacebookServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        if (! isset($container->get('Config')['auth']['facebook'])) {
            throw new \Exception('No facebook implemented in config!');
        }

        $config = $container->get('Config')['auth']['facebook'];

        $facebook = new Facebook([
            'clientId' => $config['client_id'],
            'clientSecret' => $config['client_secret'],
            'redirectUri' => $config['protocol'] . '://' . $_SERVER['HTTP_HOST'] . $config['redirect_uri'],
            'graphApiVersion' => $config['graph_api_version']
        ]);

        return new AuthFacebookService(
            $facebook,
            $container->get('Route\Session'),
            $container->get(EntityManager::class)->getRepository(BaseUser::class),
            $container->get(EntityManager::class)->getRepository(User::class),
            $container->get(ExtendedUserRepository::class)
        );
    }
}
