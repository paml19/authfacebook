<?php

namespace paml\Auth\Facebook\Factory;

use Interop\Container\ContainerInterface;
use paml\Auth\Facebook\Controller\AuthFacebookController;
use paml\Auth\Facebook\Service\AuthFacebookAdapter;
use paml\Auth\Facebook\Service\AuthFacebookService;
use paml\Auth\Service\AuthManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class AuthFacebookControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new AuthFacebookController(
            $container->get(AuthFacebookService::class),
            $container->get(AuthManager::class),
            $container->get(AuthFacebookAdapter::class),
            $container->get('Route\Session')
        );
    }
}
