<?php

namespace paml\Auth\Facebook\Factory;

use Interop\Container\ContainerInterface;
use paml\Auth\Facebook\Entity\User;
use paml\Auth\Factory\Repository\AbstractExtendUserRepository;
use Zend\ServiceManager\Factory\FactoryInterface;
use paml\Auth\Facebook\Repository\ExtendedUserRepository;

class ExtendedUserFacebookRepositoryFactory
    extends AbstractExtendUserRepository
    implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): ExtendedUserRepository {
        $invoke = parent::__invoke($container, $requestedName, $options);

        return new ExtendedUserRepository(
            $invoke['entityManager'],
            $invoke['entityManager']->getClassMetadata(User::class),
            $invoke['config']['auth']['default_role'],
            $invoke['modules']
        );
    }
}
